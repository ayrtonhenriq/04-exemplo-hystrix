package br.com.itau.gateway;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name="filmes", fallback=FilmeClientFallback.class)
public interface FilmeClient {
	
	@GetMapping
	public String buscarFilme();
}
